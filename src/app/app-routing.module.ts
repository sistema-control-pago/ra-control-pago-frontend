import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PrincipalComponent} from "./principal/principal.component";
import {LoginComponent} from "./login/login.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ProfesionComponent} from "./profesion/profesion.component";
import {EmpleadoComponent} from "./empleado/empleado.component";
import {UsuarioComponent} from "./usuario/usuario.component";
import {RolComponent} from "./rol/rol.component";

const routes: Routes = [
  {path: 'contro-pago',
    component: PrincipalComponent,
    children : [
      {path: '', component: DashboardComponent},
      {path: 'profesiones', component: ProfesionComponent},
      {path: 'empleados', component: EmpleadoComponent},
      {path: 'usuarios', component: UsuarioComponent},
      {path: 'roles', component: RolComponent},
      //{path: 'consultar-dispositivos-normativos', component: ConsultaDispositivosNormativosComponent},
      //{path: 'registrar-dispositivos-normativos', component: RegistrarDispositivosNormativosComponent},
    ]
  },
  //{path: '', component: LoginComponent}
  {path: '', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
