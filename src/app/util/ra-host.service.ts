import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class RaHostService {

  public APPLICATION_API_HOST: string = "";

  constructor() {
    switch (environment.production) {
      case true:
        // AL DEPLEGAR CAMBIAR RUTA DEFINIDO PARA BACKEND
        this.APPLICATION_API_HOST = "http://localhost:9091/api/controlpago/";
        break;
      case false:
        this.APPLICATION_API_HOST = "http://localhost:9091/api/controlpago";
        break;

    }
  }
}
