import { TestBed } from '@angular/core/testing';

import { RaRestHttpService } from './ra-rest-http.service';

describe('RaRestHttpService', () => {
  let service: RaRestHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RaRestHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
