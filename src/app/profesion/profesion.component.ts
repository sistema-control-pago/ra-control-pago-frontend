import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatTable} from "@angular/material/table";
import {ProfesionModel} from "./model/profesion.model";
import {VERSION} from "@angular/cdk";
import {FormBuilder, Validators} from "@angular/forms";
import {ProfesionService} from "./services/profesion.service";
import {NotifierService} from "angular-notifier";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";
import {FormProfesionComponent} from "./form-profesion/form-profesion.component";

@Component({
  selector: 'app-profesion',
  templateUrl: './profesion.component.html',
  styleUrls: ['./profesion.component.scss']
})
export class ProfesionComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;
  @ViewChild(MatTable, {static: true}) table!: MatTable<any>;

  public lst: any[] = [];
  public columnas: string[] = ['numeracion', 'actions', 'profesion', 'descripcion', 'estado', 'fechaCreacion', 'usuarioCreacion'];
  totalElements: number = 0;
  page: number= 0;
  size: number= 12;
  tipoParametro!: ProfesionModel;
  parametro: ProfesionModel  = new ProfesionModel();
  filtro:string='';
  version = VERSION;

  public loginForm = this.formBuilder.group({
    descripcion: ['',  Validators.required],
    tipoParametro: ['', Validators.required],
  });

  private notifier;
  constructor(
    private formBuilder: FormBuilder,
    private tipoParametroService: ProfesionService,
    notifierService: NotifierService,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public datos:any
  ) {
    this.notifier = notifierService;
  }

  ngOnInit(): void {
    this.inicializar();
  }

  inicializar() {
    this.tipoParametroService.inicializar({
      page: this.page,
      size: this.size,
      profesion: this.filtro
    }).subscribe((resp: any) => {
      this.lst = this.numeracion(resp.content.datos);
      this.totalElements = resp.content.totalElements;
      console.log(this.lst);
    });
  }

  registrar(){
    const dialogRef = this.dialog.open(FormProfesionComponent, {
      width: "40vw",
      disableClose:true,
      hasBackdrop:true,
      data: {accion: 'REGISTRAR', data: this.tipoParametro}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result!=undefined){
        this.notifier.notify('success', 'Registro exitoso!');
        this.inicializar();
      }
    });
  }

  openEdit(data: any) {
    this.tipoParametroService.ver({idProfesion: data.idProfesion}).subscribe((response: any) => {
      if (response.estado) {
        const dialogRefEdit = this.dialog.open(FormProfesionComponent, {
          width: "40vw",
          disableClose: true,
          hasBackdrop: true,
          data: {
            accion: 'EDITAR',
            data: response.content
          }
        });
        dialogRefEdit.afterClosed().subscribe(result => {
          if (result != undefined) {
            this.notifier.notify('success', 'Actualización exitoso!');
            this.inicializar();
          }
        });
      } else {
        this.notifier.notify('error', 'Error al registrar!');
      }
    });
  }

  cambiarEstado(data:any){
    console.log(data)
    if (data.estado=='1'){
      this.parametro.estado = 0;
    }else {
      this.parametro.estado = 1;
    }
    this.parametro.idProfesion = data.idProfesion;
    this.parametro.usuarioModifica = 'ROOSBELTH';
    this.tipoParametroService.cambiarEstado(this.parametro).subscribe((response: any) => {
      console.log(response);
      if (response.estado) {
        this.inicializar();
        this.notifier.notify('success', 'Cambios guardados exitosamente!');
      } else {
        this.notifier.notify('error', 'Error al guardados cambios!');
      }
    });
  }


  private numeracion(midata: any | any[]) {
    for (let i = 0; i < midata.length; i++) {
      midata[i].numeracion = ((i + 1)+(this.page*this.size));
    }
    return midata;
  }

  nextPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.size = event.pageSize;
    this.inicializar();
  }

}
